'use strict';

const minimist = require('minimist');
const fs = require('fs');

let args = minimist(process.argv.slice(2), {
    default: {
        ipsFile: 'ips.txt',
        outputFile: 'metrics.json'
    }
});

let ips = fs.readFileSync(args.ipsFile, 'utf8').split("\n");
let template = { "system.cpu.avg": 1, "system.memory.free": 1, "system.memory.cached": 1, "system.io.read": 1, "system.io.write": 1 };

let content = {};

ips.forEach(element => {
    content[element] = template;
});

fs.writeFileSync(args.outputFile, JSON.stringify(content, null, 2));