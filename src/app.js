var express = require('express');
var contentType = require('content-type');
var getRawBody = require('raw-body');

var app = express();

app.use(function (req, res, next) {
    getRawBody(req, {
        length: req.headers['content-length'],
        limit: '1mb',
        encoding: contentType.parse(req).parameters.charset
    }, function (err, string) {
        if (err) return next(err);
        req.text = string;
        next()
    })
});


var serverPressets = [
    {
        "memory": "4G",
        "CPU": "Intel(R) Core(TM) i7-6600U CPU @ 2.60GHz",
        "network": {
            "eth0": "10G",
            "wlan0": "100M"
        }
    },
    {
        "memory": "16G",
        "CPU": "Intel(R) Core(TM) i7-6600U CPU @ 2.60GHz",
        "network": {
            "eth0": "1G",
        }
    },
    {
        "memory": "12G",
        "CPU": "Intel(R) Core(TM) i5-8259U CPU @ 3.80GHz",
        "network": {
            "eth0": "1G"
        }
    },
    {
        "memory": "16G",
        "CPU": "Intel(R) Core(TM) i5-8265U CPU @ 3.90GHz",
        "network": {
            "eth0": "10G",
            "wlan0": "100M"
        }
    },
    {
        "memory": "32G",
        "CPU": "Intel(R) Core(TM) i5-9400 CPU @ 4.10GHz",
        "network": {
            "eth0": "10G",
        }
    },
];

function nameToNum(name){
    let summ = 0;
    for (var i = 0; i < name.length; i++) {
        summ += name.charCodeAt(i);
    }
    return summ;
}

app.get('/', function (req, res) {
    res.send('Inventory server');
});

app.get('/info', function (req, res) {
    let serverName = req.query['server'];

    let info = serverPressets[nameToNum(serverName.toLowerCase())%serverPressets.length];
    info["serverName"] = serverName;

    res.send(info);
});

app.post('/test', function (req, res) {
    res.send(req.ip + " " + req.text);
});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});