'use strict';

const http = require('http');
const minimist = require('minimist');
const fs = require('fs');
const sleep = require('sleep');

function sendMetric(from, to, name) {
    const postData = name + ' ' + '10' + ' ' + '1000000';

    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain',
            'Content-Length': Buffer.byteLength(postData)
        },
        localAddress: from
    };

    const req = http.request(to, options, (res) => {
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
            console.log(`STATUS: ${res.statusCode}; BODY: ${chunk}`);
        });
    });

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });

    // write data to request body
    req.write(postData);
    req.end();
}

let args = minimist(process.argv.slice(2), {
    default: {
        url: false,
        servers: 10,
        baseTime: 1000,
        metricsFile: 'metrics.json'
    }
});

if (args.url === false) {
    console.log("url required");
    process.exit(1);
}

let serversArr = JSON.parse(fs.readFileSync(args.metricsFile, 'utf8'));

for (let serverIp in serversArr) {
    if (serverIp && serversArr.hasOwnProperty(serverIp)) {
        for (let metric in serversArr[serverIp]) {
            if (serversArr[serverIp].hasOwnProperty(metric)) {
                (function (serverIp, name, tick) {
                    setTimeout(function f() {
                        sendMetric(serverIp, args.url, name)
                        // console.log(serverIp, args.url, name);
                        // sleep.msleep(30);
                        setTimeout(f, tick);
                    }, tick);
                }(serverIp, metric, parseInt(args.baseTime / serversArr[serverIp][metric])));
                sleep.msleep(parseInt(args.baseTime / 1000));
            }
        }
    }
}

// while (true) {
//     sleep.sleep(100)
// }
// sendMetric('127.0.0.2', args.url, 'sys');

