FROM node:11-alpine

WORKDIR /app
COPY app /app

CMD ["node", "src/app.js"]